module.exports = {
		
		modulo: function(x,y) {
    	
    	let json = '{x,y}';
		try {
			
			let input = JSON.parse(json);
			if (input.x || input.y == 0) {
				throw new SyntaxError("X + Y must be greater than 0");
			}
		} catch(e) {
			console.log( "JSON Error: " + e );	
		}
		try {
			
			let input = JSON.parse(json);
			if (input.x || input.y < 0) {
				throw new SyntaxError("X + Y must not be Negative Numbers");
			}
		} catch(e) {
			console.log( "JSON Error: " + e );	
		}
		try {
			
			let input = JSON.parse(json);
			if (!Number.isInteger(input.x || input.y)) {
				throw new SyntaxError("The number entered must be a whole Number");
			}
		} catch(e) {
			console.log( "JSON Error: " + e );	
		}
		 
        value = x % y;
        	return value;
    }
}
