var expect  = require('chai').expect;
var mod = require('../modulo');

it('Modulo Test', function(done) {
        var x = 11;
        var y = 5;
        var a = 1;
        expect(mod.modulo(x,y)).to.equal(a);
        done();
});
